#pragma once
#include <iostream>
#include <string>

using namespace std;
class Pilha
{
private:
	int numItens;
	string vetor;
	Pilha* pilha[15];

public:
	Pilha();
	~Pilha();
	void operator+=(Pilha pilha);
	string operator--(int vetor);
	string operator = (Pilha pilha);
	bool operator==(Pilha pilha);
	string getVetor();
	int getNumItens();
	bool operator != (Pilha *pilha);
	bool operator > (Pilha* pilha);
	bool operator >= (Pilha* pilha);
};

